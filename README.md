##### Dla aplikacji dostępne są następujące endpointy:
* `/getName` - dla przedstawienia się aplikacji
* `/getOtherName` - dla odpytania się 2-giej aplikacji z klastra o jej nazwę

Aplikacja startuje na standardowym porcie `8080`.

Konfiguracja zawiera 2 zmienne:
* `appname=default`
* `endpoint=http://localhost:8080/`

---
##### Zadanie:
Utwórz Pipeline (dla Jenkins lub Gitlab), który:
* skompiluje projekt,
* utworzy obraz dockerowy ze skompilowaną aplikacją oraz
* uruchomi aplikację
zgodnie z poniższymi wytycznymi:

1. Uruchom dwa kontenery dockerowe zawierajace aplikację z różną konfiguracją:

    * dla każdej instancji aplikacji podaj inną nazwę:
        * `appname` (np. ALFA, BETA)
    * dla każdej aplikacji wskaż wspólny endpoint:
        * `endpoint` (np. http://some-host/), którym będzie loadbalancer (patrz punkt 2)

2. Uruchom kontener dockerowy do load-balancingu (np: HaProxy, NGINX).
3. Skonfiguruj kontener z load-balancerem (z punktu 2) tak, aby:
    * aplikacje z punktu 1 komunikowały się ze sobą za pośrednictwem tego kontenera
    * po wejściu na adres http://localhost:8000 ruch został przekierowany na jeden z kontenerów, o których mowa w punkcie 1
    * po wywołaniu http://localhost:8000/getName kontener, na który zostanie przekierowany ruch zwrócił swoją nazwę zadaną w konfiguracji (`appname`)
    * po wywołaniu http://localhost:8000/getOtherName kontener zwrócił swoją nazwę zadaną w konfiguracji 
      wraz z nazwą drugiego kontenera, z którym się komunikuje (poprzez URL określony w parametrze `endpoint`).

![grafika](ryc1.png)