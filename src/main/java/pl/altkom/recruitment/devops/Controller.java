package pl.altkom.recruitment.devops;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Controller {

    @Value("${appname}")
    private String appname;

    @Value("${endpoint}")
    private String endpoint;

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/getOtherName")
    public String getOtherName(){
        ResponseEntity<String> exchange = restTemplate.exchange(
                endpoint+"/getName",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);
        return "Request from: " + appname +" -> Response is: " + exchange.getBody();
    }

    @GetMapping("/getName")
    public String getName(){
       return appname;
    }



}
